vala-demo
=========

Collection script of vala to learn vala language (Kumpulan script-script dengan bahasa vala, sebagai bahan pembelajaran)
Seperti:

- Contoh penggunaan array
- Tipe data string, int, double dll
- Contoh penggunaan fungsi pada GLib, Gio, Posix
- DateTime
- Gstream
- MySQL
- Sqlite
- OOP pada Vala
- GTK+
- etc